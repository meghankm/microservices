package com.meghan.account.model;

import java.math.BigDecimal;

public class Account {

    private Integer accountId;
    private String accountType;
    private BigDecimal balance;

    public Account() {
    }

    public Account(Integer accountId, String accountType, BigDecimal balance) {
        this.accountId = accountId;
        this.accountType = accountType;
        this.balance = balance;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public String getAccountType() {
        return accountType;
    }

    public BigDecimal getBalance() {
        return balance;
    }
}
